export enum Method {
	GET,
	POST,
	PUT,
	DELETE,
	PATCH
}

export async function fetchApi(
	url: string,
	method: Method = Method.GET,
	body: any = null
) {
	if (method === Method.GET) {
		return await fetch(url, {
			headers: { Accept: 'application/json' },
			mode: 'same-origin'
		})
	}

	return await fetch(url, {
		mode: 'same-origin',
		body: JSON.stringify(body),
		headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
		method: Method[method]
	})
}
