export class SocialMedia {
	static Facebook = 'https://www.facebook.com/anantarupastudios'
	static Youtube = 'https://www.youtube.com/c/anantarupastudios'
	static Instagram = 'https://www.instagram.com/anantarupa.studios'
}
