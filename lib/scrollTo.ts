import { animateScroll } from './animateScroll'
import { RefObject } from 'react'

const logError = () =>
	console.error(
		`Invalid element, are you sure you've provided element id or react ref?`
	)

const getElementPosition = (element: HTMLElement) => element.offsetTop

export type ScrollToType = {
	id: string
	ref?: RefObject<any>
	duration?: number
}

export const scrollTo = ({ id, ref, duration = 1000 }: ScrollToType) => {
	const initialPosition = window.scrollY

	const element = ref
		? ref.current
		: id
		? document.getElementById(id.replace('#', ''))
		: null

	if (!element) {
		logError()
		return
	}

	animateScroll({
		targetPosition: getElementPosition(element),
		initialPosition,
		duration
	})
}
