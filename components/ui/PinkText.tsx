import { NextPage } from 'next'

export const PinkText: NextPage = ({ children }) => {
	return (
		<p className="text-2xl font-bold uppercase tracking-widest text-[#d71884]">
			{children}
		</p>
	)
}
