import { scrollTo, ScrollToType } from '../../lib/scrollTo'
import { NextPage } from 'next'

export const ScrollToButton: NextPage<ScrollToType> = props => {
	const onClick = () => {
		scrollTo({ id: props.id, ref: props.ref, duration: props.duration })
	}
	return <button onClick={onClick}>{props.children}</button>
}
