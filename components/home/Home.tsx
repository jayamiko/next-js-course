import { NextPage } from 'next'
import Footer from '../layout/Footer'
import HeaderComp from '../layout/HeaderComp'
import Image from 'next/image'
import { FormEvent, useRef, useState } from 'react'
import styles from './Home.module.css'
import { PinkText } from '../ui/PinkText'
import { AppStoreBadge, GooglePlayBadge, MdiEmail } from 'ui/icon'
import isEmail from 'validator/lib/isEmail'
import { Link } from 'react-scroll'
import { fetchApi, Method } from '../../lib/fetchApi'

const googlePlayUrl =
	'https://play.google.com/store/apps/details?id=com.AnantarupaStudios.Lokapala&hl=en&gl=US'
const appStoreUrl = 'https://apps.apple.com/app/id1587114985?mt=8'
// const hcaptchaSiteKey = process.env.NEXT_PUBLIC_CAPTCHA_SITE_KEY

const Home: NextPage = () => {
	return (
		<>
			<Title />
			<AboutUs />
			<Portfolio />
			<Achievement />
			<News />
			<ContactUs />
			<Footer />
		</>
	)
}

const Title: NextPage = () => {
	return (
		<section id="title" className={styles.bgTitle}>
			<HeaderComp />
			<div className="container mx-auto grid px-12 pt-28 pb-44 md:pt-48 lg:grid-cols-3 xl:grid-cols-2 xl:pt-52">
				<div className="grid content-center items-center justify-start gap-12 lg:col-span-2 lg:justify-center xl:col-span-1">
					<h2 className="text-2xl font-bold uppercase tracking-widest text-[#d71884] md:mt-0 -mb-8">
						Welcome To
					</h2>
					<p className="text-5xl font-extrabold text-white sm:text-6xl md:text-7xl xl:text-8xl">
						Anantarupa Studios
					</p>
					<p className="text-justify text-base font-normal leading-loose tracking-wide text-gray-300 sm:text-left sm:text-xl">
						Our vision is to be the first Indonesia game developer producing
						high quality games and IPs that can answer world-class challenge.
					</p>
				</div>
			</div>
		</section>
	)
}

const AboutUs: NextPage = () => {
	return (
		<section id="about" className="relative z-40 -mt-20 pt-32 bg-white">
			<div className="container mx-auto py-6 px-12">
				<div className="grid gap-12 py-8 lg:grid-cols-2">
					<div className="grid content-start items-center justify-start gap-6">
						<PinkText>About Us</PinkText>
						<p className="text-4xl font-extrabold leading-tight text-[#29293f] sm:text-5xl 2xl:text-7xl">
							An Experienced Game Developer
						</p>
					</div>
					<div className="content-center">
						<p className="py-12 text-justify text-base font-normal leading-loose tracking-normal text-gray-500 md:py-24">
							Anantarupa is a game developer founded in 2011 based in Jakarta,
							Indonesia. We have 8 years of experience in providing B2B service
							business for custom game, AR and VR.
							<br />
							<br />
							Our dedicated and passionate team members are consist of 8
							divisions with total 55 people and keep growing. Each of our team
							have expertise and experiences for more than 8 years of game and
							tech development, always pursuing new skill levels.
							<br />
							<br />
							In 2018, We decided to upscale our business from game service
							company to IP game company starting with esports game development,
							Lokapala MOBA game.
						</p>
					</div>
				</div>
			</div>
		</section>
	)
}

const Portfolio: NextPage = () => {
	enum WorkCategory {
		ShowAll,
		InternationalIP,
		Customization,
		DigitalActivation,
		AugmentedReality,
		VirtualReality
	}
	type Type = {
		image: string
		category: WorkCategory
	}
	const work: Type[] = [
		{
			image: '/work/zee.jpg',
			category: WorkCategory.InternationalIP
		},
		{
			image: '/work/choki.PNG',
			category: WorkCategory.InternationalIP
		},
		{
			image: '/work/ar-doraemon.png',
			category: WorkCategory.InternationalIP
		},
		{
			image: '/work/2.PNG',
			category: WorkCategory.InternationalIP
		},
		{
			image: '/work/burger.png',
			category: WorkCategory.Customization
		},
		{
			image: '/work/viranopoly.png',
			category: WorkCategory.Customization
		},
		{
			image: '/work/doraemon.png',
			category: WorkCategory.Customization
		},
		{
			image: '/work/voli.png',
			category: WorkCategory.Customization
		},
		{
			image: '/work/monopoly.png',
			category: WorkCategory.Customization
		},
		{
			image: '/work/7.jpg',
			category: WorkCategory.DigitalActivation
		},
		{
			image: '/work/bca.png',
			category: WorkCategory.DigitalActivation
		},
		{
			image: '/work/7.jpg',
			category: WorkCategory.DigitalActivation
		},
		{
			image: '/work/draw-doraemon.jpg',
			category: WorkCategory.DigitalActivation
		},
		{
			image: '/work/8.png',
			category: WorkCategory.AugmentedReality
		},
		{
			image: '/work/9.jpg',
			category: WorkCategory.AugmentedReality
		},
		{
			image: '/work/pegadaian.png',
			category: WorkCategory.AugmentedReality
		},
		{
			image: '/work/pocari.png',
			category: WorkCategory.VirtualReality
		},
		{
			image: '/work/vr-shooter.png',
			category: WorkCategory.VirtualReality
		},
		{
			image: '/work/vr-1.png',
			category: WorkCategory.VirtualReality
		}
	]
	const initialWorkCount = 6

	const [workCount, setWorkCount] = useState(initialWorkCount)
	const [workCategory, setWorkCategory] = useState(WorkCategory.ShowAll)

	const showHideWork = () => {
		setWorkCount(prev => {
			if (prev === initialWorkCount) return work.length

			return initialWorkCount
		})

		if (workCount === work.length) {
			setTimeout(() => {
				document
					.getElementById('show-more')
					.scrollIntoView({ behavior: 'smooth' })
			}, 10)
		}
	}

	return (
		<section className={styles.bgPortfolio}>
			<div
				className="relative z-32 -mt-32 border-2 border-transparent pb-8 lg:px-4 lg:pt-4 lg:pb-16"
				id="ip-game"
			>
				<div className={styles.bgLokapala}>
					<div className="container mx-auto grid py-28 px-12 lg:grid-cols-3 xl:grid-cols-7">
						<div className="grid content-center items-center justify-start gap-12 lg:col-span-2 lg:justify-center xl:col-span-4">
							<PinkText>Our IP Game</PinkText>
							<p className="text-5xl font-extrabold text-white sm:text-6xl md:text-7xl xl:text-8xl">
								Lokapala : Saga of the Six Realms
							</p>
							<p className="text-justify text-base leading-loose tracking-wide text-neutral-50 sm:text-left sm:text-xl">
								The first esports game from Indonesia which that inspired by
								regional cultures to introduce unsung historical and
								mythological heroes.
							</p>
							<div className="flex flex-row items-center space-x-4">
								{/* <a href={googlePlayUrl} title="Google Play">
									<GooglePlayBadge scaleFactor={0.3} />
								</a>
								<a href={appStoreUrl} title="App Store">
									<AppStoreBadge scaleFactor={1.3} />
								</a> */}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div
				id="portfolio"
				className="container mx-auto py-10 px-12 z-20 pt-28 -mt-28"
			>
				<div className="flex flex-col items-center space-y-8 py-8">
					<PinkText>Portfolio</PinkText>
					<p className="text-4xl font-extrabold text-white sm:text-5xl md:text-6xl xl:text-7xl">
						Our Latest Work
					</p>
				</div>
				<br />
				<div className="justify-left grid items-center justify-self-center sm:flex sm:justify-center sm:space-x-6 md:flex md:justify-center md:space-x-11 lg:flex lg:justify-center lg:space-x-12">
					<a
						onClick={() => setWorkCategory(WorkCategory.ShowAll)}
						className="card-portfolio"
					>
						{workCategory === WorkCategory.ShowAll ? (
							<b className="text-white">Show All</b>
						) : (
							'Show All'
						)}
					</a>
					<a
						onClick={() => setWorkCategory(WorkCategory.InternationalIP)}
						className="card-portfolio"
					>
						{workCategory === WorkCategory.InternationalIP ? (
							<b className="text-white">International IP</b>
						) : (
							'International IP'
						)}
					</a>
					<a
						onClick={() => setWorkCategory(WorkCategory.Customization)}
						className="card-portfolio"
					>
						{workCategory === WorkCategory.Customization ? (
							<b className="text-white">Customization</b>
						) : (
							'Customization'
						)}
					</a>
					<a
						onClick={() => setWorkCategory(WorkCategory.DigitalActivation)}
						className="card-portfolio"
					>
						{workCategory === WorkCategory.DigitalActivation ? (
							<b className="text-white">Digital Activation</b>
						) : (
							'Digital Activation'
						)}
					</a>
					<a
						onClick={() => setWorkCategory(WorkCategory.AugmentedReality)}
						className="card-portfolio"
					>
						{workCategory === WorkCategory.AugmentedReality ? (
							<b className="text-white">AR</b>
						) : (
							'AR'
						)}
					</a>
					<a
						onClick={() => setWorkCategory(WorkCategory.VirtualReality)}
						className="card-portfolio"
					>
						{workCategory === WorkCategory.VirtualReality ? (
							<b className="text-white">VR</b>
						) : (
							'VR'
						)}
					</a>
				</div>
				<div className="grid grid-cols-2 place-content-stretch gap-4 py-14 md:grid-cols-2 md:grid-rows-2 lg:grid-cols-2 lg:grid-rows-1 lg:px-32 xl:grid-cols-3 xl:px-52">
					{workCategory === WorkCategory.ShowAll
						? work
								.slice(0, workCount)
								.map((x, i) => <WorkCard key={i} src={x.image} />)
						: work
								.filter(x => x.category == workCategory)
								.slice(0, workCount)
								.map((x, i) => <WorkCard key={i} src={x.image} />)}
				</div>
				<div className="flex justify-center">
					{workCategory === WorkCategory.ShowAll ? (
						<button
							id="show-more"
							onClick={showHideWork}
							className="rounded-lg border border-white px-10 py-3 text-sm font-bold uppercase text-white hover:bg-gray-500 hover:bg-opacity-25 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-200 active:bg-gray-500 active:bg-opacity-25"
						>
							Show {workCount === initialWorkCount ? 'More' : 'Less'}
						</button>
					) : (
						''
					)}
				</div>
			</div>
		</section>
	)
}

const Partner: NextPage = () => {
	const partners = [
		'/telkom.png',
		'/samsung.png',
		'/bca.png',
		'/kalbe.png',
		'/sinarmas.png'
	]

	return (
		<div className="grid grid-cols-2 place-content-stretch gap-4 border-0 border-black py-16 md:grid-cols-5 md:gap-8 md:border-b">
			{partners.map((x, i) => {
				const alt = x.split('.')[0]
				return (
					<div
						className={`relative h-20 w-auto last:col-span-2 md:last:col-span-1`}
						key={i}
					>
						<Image src={x} alt={alt} layout="fill" objectFit="contain" />
					</div>
				)
			})}
		</div>
	)
}

const WorkCard: NextPage<{ src: string }> = ({ src }) => {
	const alt = src.split('.')[0]
	return (
		<div className="relative h-40 w-32 place-self-center rounded-lg px-8 shadow-lg shadow-stone-800 hover:animate-pulse md:h-96 md:w-80 lg:h-96 lg:w-80">
			<Image
				className="rounded-lg"
				src={src}
				alt={alt}
				layout="fill"
				objectFit="cover"
				objectPosition="center"
			/>
		</div>
	)
}

const Achievement: NextPage = () => {
	type Type = {
		image: string
		title: string
		detail: string
		link: string
	}
	const types: Type[] = [
		{
			image: '/achievement/1.jpg',
			title: 'Rekor MURI',
			detail: 'The First VR Museum and VR Tourism Platform',
			link: 'https://muri.org/Website/Rekor_detail/virtualrealitymuseumdanvirtualrealitytourismplatformpertama'
		},
		{
			image: '/achievement/2.png',
			title: 'INCREFEST',
			detail: '1st Place Winner INCREFEST 2012',
			link: 'https://www.maxmanroe.com/anantarupa-studio.html'
		},
		{
			image: '/achievement/3.jpg',
			title: 'Indigo Fellowship',
			detail: '2nd Place Winner Telkom Indigo Fellowship 2011',
			link: 'https://tekno.kompas.com/read/2011/11/27/11464637/Inilah.Jawara.Indigo.Fellowship.2011?fbclid=IwAR0KaBDdRbh9RdoYfozz7w97RygOOxK4p1g6cioHFVf03SwKmrGSfNrUUYA'
		}
	]
	return (
		<section id="achievement" className={styles.bg4}>
			<div className="container mx-auto py-6 px-12">
				<div className="flex flex-col items-start space-y-8 py-12">
					<PinkText>Achievements</PinkText>
					<p className="text-4xl font-extrabold leading-tight tracking-tight text-white sm:text-5xl xl:text-6xl">
						Our Achievements
					</p>
				</div>
				<div className="grid rounded border border-gray-800 bg-white lg:grid-cols-3 lg:border-2">
					{types.map(({ image, title, detail, link }, i) => {
						return (
							<div
								key={i}
								className="grid gap-8 border-b border-inherit px-14 pt-8 pb-14 last:border-b-0 lg:border-r-2 lg:border-b-0 lg:last:border-r-0"
							>
								{/*<p className="text-4xl sm:text-5xl md:text-6xl 2xl:text-8xl font-extrabold text-[#29293f] tracking-tight leading-tight">*/}
								{/*	{image}*/}
								{/*</p>*/}
								<div className="relative h-48 w-full place-self-center">
									<Image
										src={image}
										alt={title}
										layout="fill"
										objectFit="contain"
										objectPosition="center"
									/>
								</div>
								<p className="text-lg font-bold tracking-wide md:text-xl lg:text-2xl">
									{title}
								</p>
								<p className="text-base leading-relaxed tracking-wide text-gray-400 sm:text-lg md:text-xl">
									{detail}
								</p>
								<a
									href={link}
									target="_blank"
									rel="noopener noreferrer"
									className="text-black hover:text-slate-600"
								>
									Learn More
								</a>
							</div>
						)
					})}
				</div>
			</div>
		</section>
	)
}

const ContactUs: NextPage = () => {
	// const captchaRef = useRef<HCaptcha>(null)
	const nameRef = useRef<HTMLInputElement>()
	const emailRef = useRef<HTMLInputElement>()
	const subjectRef = useRef<HTMLInputElement>()
	const messageRef = useRef<HTMLTextAreaElement>()

	const [success, setSuccess] = useState(false)
	const [nameError, setNameError] = useState<string>(null)
	const [emailError, setEmailError] = useState<string>(null)
	const [subjectError, setSubjectError] = useState<string>(null)
	const [messageError, setMessageError] = useState<string>(null)

	const submit = async (e: FormEvent) => {
		e.preventDefault()
		let isValid = true
		const name = nameRef.current.value
		const email = emailRef.current.value
		const subject = subjectRef.current.value
		const message = messageRef.current.value

		setNameError(null)
		setEmailError(null)
		setSubjectError(null)
		setMessageError(null)

		if (name.trim().length <= 0) {
			setNameError('Name cannot be empty')
			isValid = false
		} else if (name.length < 3) {
			setNameError('Name required 3 characters')
			isValid = false
		}

		if (!isEmail(email)) {
			setEmailError('Email is Invalid')
			isValid = false
		}

		if (!subject || subject.trim().length <= 0) {
			setSubjectError('Subject cannot be empty')
			isValid = false
		} else if (subject.length < 3) {
			setSubjectError('Subject required 3 characters')
			isValid = false
		}

		if (!message || message.trim().length <= 0) {
			setMessageError('Message cannot be empty')
			isValid = false
		} else if (message.length < 10) {
			setMessageError('Message required 10 characters')
			isValid = false
		}

		if (!isValid) return

		captchaRef.current.execute()
	}

	const onVerifyCaptcha = async (token: string) => {
		const name = nameRef.current.value
		const email = emailRef.current.value
		const subject = subjectRef.current.value
		const message = messageRef.current.value

		await fetchApi('/api/contact', Method.POST, {
			token,
			email,
			name,
			subject,
			message
		})

		setTimeout(() => setSuccess(false), 3000)
		setSuccess(true)

		nameRef.current.value = ''
		emailRef.current.value = ''
		subjectRef.current.value = ''
		messageRef.current.value = ''

		captchaRef.current.resetCaptcha()
	}

	const InvalidText: NextPage = ({ children }) => {
		return (
			<div className="mt-1 pl-2">
				<span className="text-sm text-red-500">{children}</span>
			</div>
		)
	}

	return (
		<section id="contact" className={styles.bg5}>
			<div className="container mx-auto grid gap-y-4 px-12 pb-24 lg:grid-cols-5 lg:grid-rows-none lg:pb-32 -mt-28 pt-32">
				<div className="flex flex-col items-start justify-center space-y-8 py-12 lg:col-span-2 lg:justify-start">
					<PinkText>Contact Us</PinkText>
					<p className="text-5xl font-extrabold leading-tight tracking-tight text-[#29293f] sm:text-5xl xl:text-6xl 2xl:text-7xl">
						How can we help you?
					</p>
					<a
						href="mailto:info@anantarupa.com"
						className="flex flex-row items-center space-x-8 px-0 py-3 hover:rounded-lg hover:text-black hover:drop-shadow-lg lg:px-6"
						title="Get In Touch"
					>
						{/* <MdiEmail className="h-12 w-12 rounded-full bg-[#D71985] p-2.5 text-white" /> */}
						<p className="text-xl font-medium text-[#29293f] underline">
							info@anantarupa.com
						</p>
					</a>
				</div>
				<div className="flex flex-col justify-center px-4 lg:col-span-2 lg:col-start-4 mt-11">
					<p className="text-3xl font-bold text-[#29293f]">Get In Touch</p>
					<form
						onSubmit={submit}
						className="mt-4 flex flex-col space-y-2 text-[#29293f]"
					>
						<div>
							<input
								ref={nameRef}
								className="input-contact"
								type="text"
								placeholder="Your Name"
							/>
							{nameError && <InvalidText>{nameError}</InvalidText>}
						</div>
						<div>
							<input
								ref={emailRef}
								className="input-contact"
								type="text"
								placeholder="Your Email"
							/>
							{emailError && <InvalidText>{emailError}</InvalidText>}
						</div>
						<div>
							<input
								ref={subjectRef}
								className="input-contact"
								type="text"
								placeholder="Subject"
							/>
							{subjectError && <InvalidText>{subjectError}</InvalidText>}
						</div>
						<div>
							<textarea
								rows={6}
								ref={messageRef}
								className="input-contact resize-none"
								placeholder="Message"
							/>
							{messageError && <InvalidText>{messageError}</InvalidText>}
						</div>
						{success && (
							<span className="mb-4 rounded-lg bg-green-200 px-6 py-3 text-base font-medium text-green-600">
								Thank You for Contacting Us.
							</span>
						)}
						{/* <HCaptcha
							id="captcha"
							sitekey={hcaptchaSiteKey}
							size="invisible"
							ref={captchaRef}
							onVerify={onVerifyCaptcha}
						/> */}
						<div>
							<button className="rounded-lg bg-[#D71985] px-6 py-3 text-lg font-bold text-white">
								Submit Now
							</button>
						</div>
					</form>
				</div>
			</div>
		</section>
	)
}

const News: NextPage = () => {
	const blogs: NewsType[] = [
		{
			image: '/news/1.jpg',
			category: 'Events',
			title: 'Jakarta Culture ARhibition 2012',
			date: '5 Nov, 2021',
			url: 'https://edukasi.kompas.com/read/2012/11/30/09140683/Augmented.Reality.Bantu.Pelajari.Budaya.Indonesia'
		},
		{
			image: '/news/2.png',
			category: 'Events',
			title: 'EXPO Milano 2015: the top 10 reasons to visit',
			date: '29 Oct, 2021',
			url: 'https://en.antaranews.com/news/98933/indonesia-offers-virtual-tour-at-world-expo-milano'
		},
		{
			image: '/news/3.png',
			category: 'Events',
			title: 'Mengenal Majapahit Lewat Media Perangkat Oculus Rift',
			date: '21 Oct, 2021',
			url: 'https://nationalgeographic.grid.id/read/13290611/mengenal-majapahit-lewat-media-perangkat-oculus-rift'
		}
	]
	return (
		<section className={styles.bg5}>
			<div className="container mx-auto px-12 pt-8 pb-12 lg:pb-16 relative">
				<div className="flex flex-col items-start space-y-8 py-12">
					<PinkText>Our Blog</PinkText>
					<p className="text-5xl font-extrabold leading-tight tracking-tight text-[#29293f] sm:text-5xl xl:text-6xl 2xl:text-7xl">
						Latest News
					</p>
				</div>
				<div className="grid place-content-center gap-8 pt-8 lg:grid-cols-3 lg:gap-6 xl:gap-8">
					{blogs.map((x, i) => (
						<NewsCard key={i} {...x} />
					))}
				</div>
			</div>
		</section>
	)
}

type NewsType = {
	category: string
	title: string
	date: string
	image: string
	url: string
}

const NewsCard: NextPage<NewsType> = ({
	image,
	category,
	title,
	date,
	url
}) => {
	return (
		<div className="grid gap-4">
			<a href={url} title={title} className="relative h-80 w-auto lg:h-64">
				<Image
					className="rounded-xl"
					src={image}
					alt={title}
					layout="fill"
					objectFit="cover"
					objectPosition="top"
				/>
			</a>
			<p className="text-base font-medium tracking-wide sm:text-lg">
				{category}
			</p>
			<a
				href={url}
				title={title}
				className="text-lg font-bold leading-tight tracking-tight text-[#29293f] hover:text-gray-500 sm:text-xl md:text-2xl"
			>
				{title}
			</a>
			<p className="text-base tracking-wide text-gray-500">{date}</p>
		</div>
	)
}

export default Home
