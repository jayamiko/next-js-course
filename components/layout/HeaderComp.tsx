import { useState, useEffect } from 'react'
import { Popover, Transition } from '@headlessui/react'
import { MenuIcon, XIcon } from '@heroicons/react/outline'
import Image from 'next/image'
import Link from 'next/link'
import { ScrollToButton } from '../ui/ScrollToButton'
import { Fragment } from 'react'
import { NextPage } from 'next'

const Header: NextPage = () => {
	const [isScroll, setIsScroll] = useState(0)

	useEffect(
		function onFirstMount() {
			function onScroll() {
				setIsScroll(window.scrollY)
			}
			window.addEventListener('scroll', onScroll)
		},
		[isScroll]
	)

	return (
		<div
			className={
				isScroll
					? 'bg-primary fixed z-50 w-full transition duration-300'
					: 'from-primary fixed w-full bg-gradient-to-b to-transparent'
			}
		>
			<Popover className="z-2 relative">
				<div className="container mx-auto px-8 py-8">
					<div className="flex items-center md:space-x-10">
						<div className="flex flex-grow items-center lg:flex-1">
							<Link href="/">
								<a>
									<span className="sr-only">Lokapala</span>
									<div>
										<Image
											src="/anantarupa-logo.png"
											alt="lokapala"
											height={70}
											width={70}
										/>
									</div>
								</a>
							</Link>
						</div>
						<div className="-my-2 -mr-2 lg:hidden">
							<Popover.Button className="inline-flex items-center justify-center rounded-md border p-2 text-white hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-200">
								<span className="sr-only">Open menu</span>
								<MenuIcon className="h-6 w-6" aria-hidden="true" />
							</Popover.Button>
						</div>
						<Popover.Group as="nav" className="hidden space-x-10 lg:flex">
							<ScrollToButton id="#about">
								<a className="nav-text">About Us</a>
							</ScrollToButton>
							<ScrollToButton id="#ip-game">
								<a className="nav-text">Our IP Game</a>
							</ScrollToButton>
							<ScrollToButton id="#portfolio">
								<a className="nav-text">Portfolio</a>
							</ScrollToButton>
							<ScrollToButton id="#achievement">
								<a className="nav-text">Achievements</a>
							</ScrollToButton>
							<ScrollToButton id="#contact">
								<a className="nav-text">Contact Us</a>
							</ScrollToButton>
						</Popover.Group>
					</div>
				</div>
				<Transition
					as={Fragment}
					enter="duration-200 ease-out"
					enterFrom="opacity-0 scale-95"
					enterTo="opacity-100 scale-100"
					leave="duration-100 ease-in"
					leaveFrom="opacity-100 scale-100"
					leaveTo="opacity-0 scale-95"
				>
					<Popover.Panel
						focus
						className="absolute inset-x-0 top-0 origin-top-right transform p-2 transition lg:hidden"
					>
						{({ close }) => (
							<div className="z-10 divide-y-2 divide-gray-50 rounded-lg bg-white px-5 shadow-lg ring-1 ring-black ring-opacity-5">
								<div className="border-b-2 border-b-gray-300 pt-5 pb-3">
									<div className="flex items-center justify-between">
										<Link href="/">
											<a>
												<Image
													src="/anantarupa-logo.png"
													alt="lokapala"
													height={80}
													width={80}
												/>
											</a>
										</Link>
										<div className="-mr-2">
											<Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-200">
												<span className="sr-only">Close menu</span>
												<XIcon className="h-6 w-6" aria-hidden="true" />
											</Popover.Button>
										</div>
									</div>
								</div>
								<div className="py-3">
									<div className="grid gap-2">
										<ScrollToButton id="#about">
											<a onClick={() => close()}>
												<p className="nav-menu__text">About Us</p>
											</a>
										</ScrollToButton>
										<ScrollToButton id="#ip-game">
											<a onClick={() => close()}>
												<p className="nav-menu__text">Our IP Game</p>
											</a>
										</ScrollToButton>
										<ScrollToButton id="#portfolio">
											<a onClick={() => close()}>
												<p className="nav-menu__text">Portfolio</p>
											</a>
										</ScrollToButton>
										<ScrollToButton id="#achievement">
											<a onClick={() => close()}>
												<p className="nav-menu__text">Achievements</p>
											</a>
										</ScrollToButton>
										<ScrollToButton id="#contact">
											<a onClick={() => close()}>
												<p className="nav-menu__text">Contact Us</p>
											</a>
										</ScrollToButton>
									</div>
								</div>
							</div>
						)}
					</Popover.Panel>
				</Transition>
			</Popover>
		</div>
	)
}
export default Header
