import { NextPage } from 'next'
import { ScrollToButton } from '../ui/ScrollToButton'
import styles from '../home/Home.module.css'
// import { FacebookIcon, InstagramIcon, YoutubeIcon } from 'ui/icon'
// import { SocialMedia } from '../../lib/SocialMedia'
import Link from 'next/link'

const Footer: NextPage = () => {
	return (
		<section className={styles.bg6}>
			<div className="container mx-auto px-12">
				<div className="grid place-content-center gap-8 border-b border-gray-700 py-20 lg:grid-cols-4">
					<div className="text-center">
						<p className="text-3xl font-bold leading-tight tracking-tight text-white">
							Anantarupa Studios
						</p>
					</div>
					<div className="place-self-center text-center lg:col-span-2 lg:text-left">
						<p className="mb-6 text-lg font-bold uppercase tracking-widest text-[#d71884]">
							Menu
						</p>
						<ul className="text-lg tracking-wider text-white">
							<li className="my-4">
								<ScrollToButton id="about">About Us</ScrollToButton>
							</li>
							<li className="my-4">
								<ScrollToButton id="ip-game">Our IP Game</ScrollToButton>
							</li>
							<li className="my-4">
								<ScrollToButton id="portfolio">Portfolio</ScrollToButton>
							</li>
							<li className="my-4">
								<ScrollToButton id="achievement">Achievements</ScrollToButton>
							</li>
							<li className="my-4">
								<ScrollToButton id="contact">Contact Us</ScrollToButton>
							</li>
						</ul>
					</div>
					{/*<div className="text-center lg:text-left">*/}
					{/*	<p className="text-lg uppercase text-[#d71884] font-bold tracking-widest mb-6">*/}
					{/*		Service*/}
					{/*	</p>*/}
					{/*	<ul className="font-sans text-lg text-white tracking-wider">*/}
					{/*		<li className="my-4">*/}
					{/*			Design*/}
					{/*		</li>*/}
					{/*		<li className="my-4">*/}
					{/*			Development*/}
					{/*		</li>*/}
					{/*		<li className="my-4">*/}
					{/*			Marketing*/}
					{/*		</li>*/}
					{/*		<li className="my-4">*/}
					{/*			See More*/}
					{/*		</li>*/}
					{/*	</ul>*/}
					{/*</div>*/}
					{/* <div className="flex flex-row justify-around space-x-2.5 text-white lg:justify-end lg:space-x-4">
						<a
							href={SocialMedia.Facebook}
							title="Facebook"
							target="_blank"
							rel="noreferrer"
						>
							<FacebookIcon className="btn-sosial-media" />
						</a>
						<a
							href={SocialMedia.Instagram}
							title="Instagram"
							target="_blank"
							rel="noreferrer"
						>
							<InstagramIcon className="btn-sosial-media" />
						</a>
						<a
							href={SocialMedia.Youtube}
							title="Youtube"
							target="_blank"
							rel="noreferrer"
						>
							<YoutubeIcon className="btn-sosial-media" />
						</a>
					</div> */}
				</div>
				<div className="flex flex-col items-center space-y-6 py-14 text-center text-base text-gray-300 lg:flex-row lg:items-end lg:space-x-16 lg:text-lg">
					<p className="text-gray-400 lg:flex-grow lg:text-left">
						Copyright © 2022 Anantarupa Studios. All Rights Reserved.
					</p>
					{/*<p>Terms of Use</p>*/}
					<Link href="/policies/privacy-policy">
						<a title="Privacy Policy">Privacy Policy</a>
					</Link>
				</div>
			</div>
		</section>
	)
}

export default Footer
