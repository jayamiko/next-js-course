const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
	darkMode: 'class',
	content: [
		'./pages/**/*.{js,ts,jsx,tsx}',
		'./components/**/*.{js,ts,jsx,tsx}'
	],
	theme: {
		extend: {
			fontFamily: {
				sans: ['Epilogue', ...defaultTheme.fontFamily.sans]
			},
			colors: {
				primary: '#29293f'
			}
		}
	},
	plugins: [require('@tailwindcss/typography')]
}
