import { NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import Header from '../components/layout/Header'

const Custom404: NextPage = () => {
	return (
		<>
			<Head>
				<title>404 Page Not Found</title>
				<meta name="description" content="404 error page" />
			</Head>
			<div className=" bg-primary">
				<section id="header404"></section>
				<Header />
				<section
					id="error404"
					className="min-h-screen bg-[url(/404-bg-sorry.webp)] bg-contain bg-[center_top_9rem] bg-no-repeat md:bg-[center_top_2rem] lg:bg-[center_top_6rem] xl:bg-[center_top_5rem]"
				>
					<div className="container mx-auto grid pt-28 sm:grid-cols-4 md:grid-cols-2 md:pt-20 lg:pt-40 xl:gap-x-72 xl:pt-40">
						<div className="mx-3 grid content-start justify-start gap-5 sm:col-span-3 sm:gap-5 md:col-span-1 md:gap-6 lg:gap-7 xl:mx-0 xl:gap-5">
							<p className="text-center text-2xl font-extrabold text-white sm:text-left sm:text-5xl md:text-4xl lg:text-6xl xl:text-7xl">
								Something Went Wrong!
							</p>
							<p className="mx-8 text-center text-base leading-loose tracking-wide text-gray-300 sm:mx-0 sm:text-left sm:text-xl md:text-sm xl:text-base">
								Whoops. Looks like this page doesn't exist. You could return to
								the homepage using the back button below.
							</p>
							<div className="grid justify-items-center sm:justify-start">
								<Link href="/" passHref>
									<button className="box-border rounded-md border border-solid px-12 py-3 text-sm font-black text-white hover:border-black hover:bg-white hover:text-black">
										<p className="hover:to-black">GO BACK</p>
									</button>
								</Link>
							</div>
						</div>
						<div className="lg:pt-68 grid content-end justify-items-center pt-5 sm:col-span-1 sm:justify-items-start sm:pt-64 md:col-span-1 md:pt-52 lg:col-span-1 lg:justify-end xl:col-span-1">
							<div>
								<p className="text-8xl font-extrabold text-[#B1B2B9] md:text-[176px] lg:text-[230px] xl:text-[350px]">
									404
								</p>
							</div>
						</div>
					</div>
				</section>
			</div>
		</>
	)
}

export default Custom404
