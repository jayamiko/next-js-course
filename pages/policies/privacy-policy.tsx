import { NextPage, GetStaticProps } from 'next'
import Head from 'next/head'
import ReactMarkdown from 'react-markdown'
import * as fs from 'node:fs'
import * as path from 'node:path'

type PrivacyPolicyProp = {
	data: string
}

const PrivacyPolicy: NextPage<PrivacyPolicyProp> = ({ data }) => {
	return (
		<>
			<Head>
				<title>Anantarupa Studios - Privacy Policy</title>
				<meta name="description" content="Anantarupa Studios Privacy Policy" />
			</Head>
			<div className="bg-neutral-200">
				<div className="container mx-auto py-16">
					<ReactMarkdown className="prose prose-sm md:prose-base lg:prose-lg mx-auto max-w-[80%] lg:max-w-[70%]">
						{data}
					</ReactMarkdown>
				</div>
			</div>
		</>
	)
}

export const getStaticProps: GetStaticProps = async () => {
	const fullPath = path.join(process.cwd(), '_policies', 'index.md')
	const markdown = fs.readFileSync(fullPath, 'utf8')
	const props: PrivacyPolicyProp = {
		data: markdown
	}
	return {
		props
	}
}

export default PrivacyPolicy
