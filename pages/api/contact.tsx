import { NextApiRequest, NextApiResponse } from 'next'
import { fetchApi, Method } from '../../lib/fetchApi'

const url = process.env.API_URL

export const handler = async (req: NextApiRequest, res: NextApiResponse) => {
	if (req.method === 'GET') {
		const { token, email, name, subject, message } = req.body

		await fetchApi(url + '/contact', Method.GET, {
			token,
			email,
			name,
			subject,
			message
		})

		res.status(200).json({ message: null })
	} else {
		res.status(405).json(null)
	}
}
