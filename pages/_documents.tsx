import Document, { Head, Html, Main, NextScript } from 'next/document'

class DefaultDocument extends Document {
	render(): JSX.Element {
		return (
			<Html
				lang="en-ID"
				className="scroll-smooth"
				style={{ scrollBehavior: 'smooth' }}
			>
				<Head>
					<link
						href="https://fonts.googleapis.com/css2?family=Epilogue:wght@400;500;600;700;800&display=swap"
						rel="stylesheet"
					/>
				</Head>
				<body className="min-h-screen bg-[#29293f]">
					<Main />
					<NextScript />
				</body>
			</Html>
		)
	}
}

export default DefaultDocument
