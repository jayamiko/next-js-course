import { NextPage } from 'next'
import Head from 'next/head'
import Home from '../components/home/Home'

const HomePage: NextPage = () => {
	const description = `Our vision is to be the first Indonesia game developer producing high 
    quality games and IPs that can answer world-class challenge.`

	return (
		<>
			<Head>
				<title>Anantarupa Studios - Home</title>
				<meta name="description" content={description} />
			</Head>
			<Home />
		</>
	)
}

export default HomePage
